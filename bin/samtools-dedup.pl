#!/usr/bin/env perl

# samtools-dedup
# authored by Samuel Barreto <samuel.barreto8@gmail.com>
# created at 2020-05-23T12:30:01+0200

use strict;
use warnings;
use v5.24;

use Cwd qw(abs_path);
use File::Basename;
use File::Copy qw(copy);
use File::Path qw(make_path);
use File::Temp qw(tempfile tempdir);
use FindBin qw($Script);
use Getopt::Long qw(GetOptions);
use POSIX qw(strftime floor ceil);
use Time::HiRes qw(time);
use List::Util qw(first);

# defaults -----------------------------------------------------------

use constant DEBUG => $ENV{'DEBUG'};
use constant EXE   => File::Basename::basename(__FILE__);
my $start_time = time();

my $arg_in;
my $arg_out;
my $arg_inplace;
my $arg_help;
my $arg_cpus;
my $arg_verbose;
my $arg_mem;

# arguments parsing --------------------------------------------------

usage(1) unless @ARGV;

GetOptions(
    "i|in=s"        => \$arg_in,
    "o|out=s"       => \$arg_out,
    "m|memory=i"    => \$arg_mem,
    "c|cpus=i"      => \$arg_cpus,
    "h|help+"       => \$arg_help,
    "v|verbose!"    => \$arg_verbose,
) or usage(1);

usage(1) if $arg_help;

$arg_verbose ||= 0;
$arg_cpus    ||= 4;
$arg_inplace ||= 0;
$arg_mem     ||= 8;

# actually memory is per thread (see https://github.com/samtools/samtools/issues/831)
$arg_mem = floor( $arg_mem / $arg_cpus );

# $arg_in = basename($arg_in);

my @exts = qw(.bam .cram .sam);
my ( $prefix, $dir, $ext ) = fileparse( $arg_in, @exts );
my $format = first { /$ext/ } @exts;
$format = uc $format;
$format = substr $format, 1;

# body ---------------------------------------------------------------

greetings();
msg("Input is $arg_in");
msg("Prefix is $prefix");
msg("Directory is $dir");
msg("Extension is $ext");
msg("Format is $format");
msg("Output is $arg_out");

my ( $fh_nsrt, $nsrt ) =
  tempfile( DIR => $dir, SUFFIX => ".nameSrt$ext", UNLINK => 1 );
my ( $fh_fmsrt, $fmsrt ) =
  tempfile( DIR => $dir, SUFFIX => ".srt$ext", UNLINK => 1 );
my $cmd =
    qq(samtools sort -@ ${arg_cpus} -m ${arg_mem}G -n -O ${format} ${arg_in} )
  . qq(| samtools fixmate -m -r -O ${format} - ${nsrt});
run_cmd($cmd);
$cmd = qq(samtools sort -@ ${arg_cpus} -m ${arg_mem}G -O $format -o ${fmsrt} ${nsrt});
run_cmd($cmd);
$cmd = qq(samtools markdup -O ${format} -s -f ${dir}/${prefix}.dup.stats ${fmsrt} ${arg_out});
run_cmd($cmd);

farewell();

# subroutines --------------------------------------------------------

# utils --------------------------------------------------------------

sub greetings {
    msg('Deduplicate reads based on read mapping');
    msg('Written by Samuel Barreto <samuel.barreto8@gmail.com>');
    msg("Licensed under GPLv3+");
    rule();
}

sub farewell {
    rule();
    msg("Done.");
}

sub msg {
    my $time = strftime "%H:%M:%S", localtime;
    my $msg  = "# [" . EXE . "] ($time): @_\n";
    print STDERR $msg;
}

sub err { msg(@_) && exit(1); }

sub dbg { msg(@_) if $ENV{'DEBUG'} or $arg_verbose; }

sub get_cmd {
    my ($cmd) = @_;
    ( my $msg = $cmd ) =~ s/[\n\r\\]//g;
    dbg("Running: '$msg'");
    return `$cmd`;
}

sub run_cmd {
    my ($cmd) = @_;
    dbg("Running: $cmd");
    system( "bash", "-c", $cmd ) == 0 or err ("Error $? running command: $!");
}

sub rule {
    my $elapsed_time =
      sprintf( "\t(Elapsed time: %.2f min)", ( time() - $start_time ) / 60 );
    msg(
"------------------------------------------------------------------------"
          . $elapsed_time );
}

sub round { sprintf( "%.2f", @_ ); }

# usage --------------------------------------------------------------

sub usage {
    my ($exitcode) = @_;
    my $EXE = EXE;

    # FIXME fill usage
    my $usage = <<EOF;
Usage: $EXE [options]

Options:
  -i,--in                        Input file in sam, bam, or cram format
  -o,--out                       Output file (same extension)
  -m,--memory 8                  Memory allocated to samtools
  -c,--cpus 4                    Number of allocated cpus
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -h,--help                      Display this message.
EOF

    print STDERR $usage;
    exit($exitcode);
}

__END__
