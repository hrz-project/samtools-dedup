# al_samtools-deduplication_2020-05-23
# created at <2020-05-23T12:30:01+0200>
# authored by: <Samuel Barreto>

SHELL=bash

ifndef PREFIX
PREFIX = /usr/local
endif

.PHONY: install
install:
	ln -s $(PWD)/bin/samtools-dedup.pl $(PREFIX)/bin/samtools-dedup

.PHONY: doc
doc: README.pdf

README.pdf: doc/workflow.png

%.pdf: %.md
	pandoc -i $< -o $@ \
		--template eisvogel \
		--listings \
		--filter pandoc-citeproc \
		--bibliography "$$HOME/Dropbox/bibliography/references.json" \
		--csl "$$HOME/Dropbox/bibliography/styles/chicago-author-date.csl"

doc/%.png: doc/%.dot ; dot -Tpng -Gdpi=300 $< > $@
