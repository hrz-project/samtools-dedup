---
title: Samtools mark duplicates
author: samuel barreto
date: 2020-05-26T10:59:18+0200
nofoot: true
---

# samtools-deduplication

Deduplicate reads based on read mapping.

## Installation

```bash
# install to /usr/local/bin/samtools-dedup
make install
# install to ~/.local/bin/samtools-dedup
PREFIX=~/.local/ make install
```

## Usage

```bash
$ samtools-dedup --help
```

```
Usage: samtools-dedup [options]

Options:
  -i,--in                        Input file in sam, bam, or cram format
  -o,--out                       Output file (same extension)
  -m,--memory 8                  Memory allocated to samtools
  -c,--cpus 4                    Number of allocated cpus
  -v,--verbose,--no-verbose      Toggle verbose mode on/off
  -h,--help                      Display this message.
```

## Dependencies

- [samtools](https://www.htslib.org/) version >= 1.10.

## Methods

![workflow](doc/workflow.png)

# License

[![gpl-v3](https://www.gnu.org/graphics/gplv3-88x31.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
